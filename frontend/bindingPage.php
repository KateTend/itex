<!DOCTYPE html>
<html>
<head>
	<title>Bindings</title>
	<?php
		include_once '../backend/mainLogic/select.php';
		include_once '../backend/mainLogic/bindings.php';
		include_once '../backend/mainLogic/connection.php';
	?>
</head>
<body>
	<form action='../backend/bindingProcessing/bindLessonWithGroup.php' method='post'>
		<select name='lessonsTitles'>		
			<?php
				$dbh = connectToDb();
				$lessons = selectAllLessons($dbh);
				foreach($lessons as $lesson) {
					echo "<option value='".$lesson['discipline']."'>".$lesson['discipline']."</option>";
				}				
				disconnectFromDb($dbh);
			?>				
		</select>
		<div>add to with</div>
		<select name='groupsTitles'>		
			<?php
				$dbh = connectToDb();
				$groups = selectAllGroups($dbh);
				foreach($groups as $group) {
					echo "<option value='".$group['title']."'>".$group['title']."</option>";
				}				
				disconnectFromDb($dbh);
			?>				
		</select>
		<div><input type='submit'/><div>
	</form>
	
	<br>
	<form action='../backend/bindingProcessing/bindLessonWithTeacher.php' method='post'>
		<select name='lessonsTitles'>		
			<?php
				$dbh = connectToDb();
				$lessons = selectAllLessons($dbh);
				foreach($lessons as $lesson) {
					echo "<option value='".$lesson['discipline']."'>".$lesson['discipline']."</option>";
				}				
				disconnectFromDb($dbh);
			?>				
		</select>
		<div>add to with</div>
		<select name='teachersNames'>		
			<?php
				$dbh = connectToDb();
				$teachers = selectAllTeachers($dbh);
				foreach($teachers as $teacher) {
					echo "<option value='".$teacher['name']."'>".$teacher['name']."</option>";
				}				
				disconnectFromDb($dbh);
			?>				
		</select>
		<div><input type='submit'/><div>
	</form>
		<div><a href='mainPage.html'>Назад на головну</a></div>
</body>
</html>