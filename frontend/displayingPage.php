<!DOCTYPE html>
<html ng-app="displayingApp" xlmns="http://www.w3.org/1999/xhtml>
<head
	<title>Displaying</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<meta http-equiv="Pragma" content="no-cache" />
	<?php
		include_once '../backend/mainLogic/select.php';
		include_once '../backend/mainLogic/connection.php';
	?>
</head>
<body ng-controller="displayingController">
	<form name="bdContentForm" method="_POST">
		<select id="auditoriumSelect" name='auditoriumsNumbers' ng-model="auditorium">		
			<?php
				$dbh = connectToDb();
				$auditoriums = selectAllByAuditoriums($dbh);
				foreach($auditoriums as $auditorium) {
					echo "<option value='".$auditorium['auditorium']."'>".$auditorium['auditorium']."</option>";
				}				
				disconnectFromDb($dbh);
			?>		
		</select>		
		<select id="groupSelect" name='groupsTitles' ng-model="group">
			<?php
				$dbh = connectToDb();
				$groups = selectAllGroups($dbh);
				foreach($groups as $group) {
					echo "<option value='".$group['title']."'>".$group['title']."</option>";
				}				
				disconnectFromDb($dbh);
			?>				
		</select>
		<select id="teacherSelect" name='teachersNames' ng-model="teacher">		
			<?php
				$dbh = connectToDb();
				$teachers = selectAllTeachers($dbh);
				foreach($teachers as $teacher) {
					echo "<option value='".$teacher['name']."'>".$teacher['name']."</option>";
				}				
				disconnectFromDb($dbh);
			?>				
		</select>
		<div></div>
		<br><input type='button' name="loadByAuditoriumHtml" value="Load HTML" ng-click="getByAuditorium(auditorium)" />
		<input type="button" name="loadByGroupXml" value="Load XML" ng-click="getByGroup(group)"/>
		<input type="button" name="loadByTeachersJson" value="Load JSON" ng-click="getByTeacher(teacher)"/>

		<div>
			For auditorium:
			<ul id="lessonListForAuditorium" ng-repeat="item in auditoriums.items">
				<li>{{item}}</li>
			</ul>
		</div>
		<div>
			For group:
			<ul id="lessonListForGroup" ng-repeat="item in groups.items">
				<li>{{item}}</li>
			</ul>
		</div>
		<div>
			For teacher:
			<ul id="lessonListForTeacher" ng-repeat="item in teachers.items">
				<li>{{item}}</li>				
			</ul>
		</div>
	</form>

	<script type="text/javascript" src="angular.min.js"></script>
	<script type="text/javascript">
	var auditoriumArray = {
  		items: []
	};
	var groupArray = {
  		items: []
	};
	var teacherArray = {
  		items: []
	};
		var displayingApp = angular.module("displayingApp", []);
		displayingApp.controller("displayingController", function ($scope, $http) {
			$scope.getByAuditorium = function(auditorium) {
				$http({method:'GET', url:'../backend/mainLogic/getLessonsForAuditoriumHtml.php', params: {'auditoriumsNumbers':auditorium}}).success(function(data) {
						auditoriumArray.items = data;
						$scope.auditoriums = auditoriumArray;
					})
			};
			$scope.getByGroup = function(group) {
				$http({method:'GET', url:'../backend/mainLogic/getLessonsForGroupHtml.php', params: {'groupsTitles':group}}).success(function(data) {
						groupArray.items = data;
						$scope.groups = groupArray;
					})
			};
			$scope.getByTeacher = function(teacher) {
				$http({method:'GET', url:'../backend/mainLogic/getLessonsForTeacherHtml.php', params: {'teachersNames':teacher}}).success(function(data) {
						teacherArray.items = data;
						$scope.teachers = teacherArray;
					})
			}
		});	
	</script>
	<div><a href='mainPage.html'>Back to main</a></div>
</body>
</html>