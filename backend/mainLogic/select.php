<?php

	// group functions 
	function selectGroupById($dbh, $ID_Groups)
	{
	$sth = $dbh->prepare('SELECT * FROM groups WHERE ID_Groups = ?');
	$sth->execute(array($ID_Groups));
	$group = $sth->fetch(PDO::FETCH_LAZY);
	return $group;
	}
		
	function selectGroupByTitle($dbh, $title)
	{
	$sth = $dbh->prepare('SELECT * FROM groups WHERE title = ?');
	$sth->execute(array($title));
	$group = $sth->fetch(PDO::FETCH_LAZY);
	return $group;
	}
	
	// lesson functions	
	function selectLessonById($dbh, $ID_Lesson)
	{
	$sth = $dbh->prepare('SELECT * FROM lesson WHERE ID_Lesson = ?');
	$sth->execute(array($ID_Lesson));
	$lesson = $sth->fetch(PDO::FETCH_LAZY);
	return $lesson;		
	}
	
	function selectLessonByWeekDay($dbh, $week_day)
	{
	$sth = $dbh->prepare('SELECT * FROM lesson WHERE week_day = ?');
	$sth->execute(array($week_day));
	$lesson = $sth->fetchAll();
	return $lesson;
	}
	
	function selectLessonByAuditorium($dbh, $auditorium)
	{
	$sth = $dbh->prepare('SELECT * FROM lesson WHERE auditorium = ?');
	$sth->execute(array($auditorium));
	$lesson = $sth->fetchAll();
	return $lesson;
	}
	
	function selectLessonByDisciple($dbh, $discipline)
	{
	$sth = $dbh->prepare('SELECT * FROM lesson WHERE discipline = ?');
	$sth->execute(array($discipline));
	$lesson = $sth->fetch(PDO::FETCH_LAZY);
	return $lesson;
	}
	
	function selectLessonByLessonNumber($dbh, $lesson_number)
	{
	$sth = $dbh->prepare('SELECT * FROM lesson WHERE lesson_number = ?');
	$sth->execute(array($lesson_number));
	$lesson = $sth->fetchAll();	
	return $lesson;
	}
	
	
	function selectAllByAuditoriums($dbh) {
		$dbh ->query("SET CHARACTER SET utf8");
		$auditorium = $dbh->query('SELECT * FROM lesson GROUP BY auditorium');
		return $auditorium;
	}
	
	function selectLessonsForAuditorium($dbh, $auditorium) {
		$sth = $dbh->prepare('SELECT * FROM lesson WHERE auditorium = ?');
		$sth->execute(array($auditorium));
		$lessonsForAuditorium = $sth->fetchAll();
		return $lessonsForAuditorium;
	}
	
	function selectLessonsForGroup($dbh, $ID_Groups)
	{
		$sth = $dbh->prepare('SELECT * FROM lesson WHERE ID_Lesson IN (SELECT FID_Lesson2 FROM lesson_group WHERE FID_Groups = ?)');
		$sth->execute(array($ID_Groups));
		$lessonsForGroup = $sth->fetchAll();
		return $lessonsForGroup;
	}
	
	function selectLessonsForTeacher($dbh, $ID_Teacher) {
		$sth = $dbh->prepare('SELECT * FROM lesson WHERE ID_Lesson IN (SELECT FID_Lesson1 FROM lesson_teacher WHERE FID_Teacher = ?)');
		$sth->execute(array($ID_Teacher));
		$lessonsForTeacher = $sth->fetchAll();
		return $lessonsForTeacher;
	}
	
	function isNotExistLessonForTeacher($dbh, $ID_Lesson, $ID_Teacher) {
		$sth = $dbh->prepare('SELECT * FROM lesson_teacher WHERE FID_Teacher = ? AND FID_Lesson1 = ?');
		$sth->execute(array($ID_Teacher, $ID_Lesson));
		$lessons = $sth->fetch(PDO::FETCH_LAZY);
		return empty($lessons);
	}
	
	function isNotExistLessonForGroup($dbh, $ID_Lesson, $ID_Groups) {
		$sth = $dbh->prepare('SELECT * FROM lesson_group WHERE FID_Groups = ? AND FID_Lesson2 = ?');
		$sth->execute(array($ID_Groups, $ID_Lesson));
		$lessons = $sth->fetch(PDO::FETCH_LAZY);
		return empty($lessons);
	}
	
	// teacher functions
	function selectTeacherById($dbh, $ID_Teacher)
	{
	$sth = $dbh->prepare('SELECT * FROM teacher WHERE ID_Teacher = ?');
	$sth->execute(array($ID_Teacher));
	$teacher = $sth->fetchAll();
	return $teacher;
	}
	function selectTeacherByName($dbh, $name)
	{
	$sth = $dbh->prepare('SELECT * FROM teacher WHERE name = ?');
	$sth->execute(array($name));
	$teacher = $sth->fetch(PDO::FETCH_LAZY);
	return $teacher;
	}
	
	// select all entites
	
	function selectAllLessons($dbh) {
		$sql = 'SELECT * FROM lesson';
		$dbh ->query("SET CHARACTER SET utf8");
		$teachers = $dbh->query($sql);
		return $teachers;
	}
	
	function selectAllGroups($dbh) {
		$dbh ->query("SET CHARACTER SET utf8");
		$teachers = $dbh->query('SELECT * FROM groups');
		return $teachers;
	}
	
	function selectAllTeachers($dbh) {
		$dbh ->query("SET CHARACTER SET utf8");
		$teachers = $dbh->query('SELECT * FROM teacher');
		return $teachers;
	}

?>