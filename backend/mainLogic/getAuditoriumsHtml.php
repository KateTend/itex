<?php
	include_once 'select.php';
	include_once 'connection.php';
?>
<?php
	$dbh = connectToDb();
	$auditoriums = selectAllByAuditoriums($dbh);
	foreach($auditoriums as $auditorium) {
		echo "<option value='".$auditorium['auditorium']."'>".$auditorium['auditorium']."</option>";
	}				
	disconnectFromDb($dbh);
?>