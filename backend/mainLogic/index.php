<?php
	include_once 'includes.php';	
	
	$dbh = connectToDb();

	addLessonToGroup($dbh, '201', '101');
	addLessonToGroup($dbh, '201', '102');
	addLessonToGroup($dbh, '201', '103');
	addLessonToGroup($dbh, '201', '104');

	addLessonToTeacher($dbh, '201', '301');
	addLessonToTeacher($dbh, '202', '302');
	addLessonToTeacher($dbh, '202', '301');
	addLessonToTeacher($dbh, '203', '302');
	addLessonToTeacher($dbh, '201', '303');
	addLessonToTeacher($dbh, '202', '303');
	
	createGroup($dbh, '105', 'tpks');
	
	/*$lessons = selectLessonsForTeacher($dbh, '301');
	
	foreach($lessons as $row) {
		print_r($row);
		print_r('<br>');
	}*/
	
	
	disconnectFromDb($dbh);
?>