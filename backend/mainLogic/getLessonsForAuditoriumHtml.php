<?php
	include_once 'select.php';
	include_once 'connection.php';
?>
<?php
	$dbh = connectToDb();
	if (array_key_exists('auditoriumsNumbers', $_GET)) {
		$selectedAuditoriumNumber = $_GET['auditoriumsNumbers'];
		if (!empty($selectedAuditoriumNumber)) {
			$selectedLessonsByAuditorium = selectLessonByAuditorium($dbh, $selectedAuditoriumNumber);
			$resultArray = array();
			foreach($selectedLessonsByAuditorium as $lesson) {
				array_push($resultArray, $lesson['discipline'].", ".$lesson['week_day']. ", ".$lesson['lesson_number']);
			}
			echo json_encode($resultArray);
		}
	}	
	disconnectFromDb($dbh);
?>