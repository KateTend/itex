<?php
	include_once 'select.php';
	
	function addLessonToGroup($dbh, $lessonId, $groupId) {
		try {
			$group = selectGroupById($dbh, $groupId);
			$lesson = selectLessonById($dbh, $lessonId);
			$isNotExistLessonForGroup = isNotExistLessonForGroup($dbh, $lessonId, $groupId);
			if (!is_null($group) && !is_null($lesson) && $isNotExistLessonForGroup) {
				$dbh->query("SET CHARACTER SET utf8");
				$sth = $dbh->prepare('INSERT INTO lesson_group(FID_Lesson2, FID_Groups) VALUES(?, ?)');
				$sth->execute(array($lessonId, $groupId));
			}			
		}
		catch(PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}	
	}
	
	function addLessonToTeacher($dbh, $lessonId, $teacherId) {
		try {
			$teacher = selectTeacherById($dbh, $teacherId);
			$lesson = selectLessonById($dbh, $lessonId);
			$isNotExistLessonForTeacher = isNotExistLessonForTeacher($dbh, $lessonId, $teacherId);
			if (!is_null($teacher) && !is_null($lesson) && $isNotExistLessonForTeacher) {
				$dbh->query("SET CHARACTER SET utf8");
				$sth = $dbh->prepare('INSERT INTO lesson_teacher(FID_Teacher, FID_Lesson1) VALUES(?, ?)');
				$sth->execute(array($teacherId, $lessonId));
			}			
		}
		catch(PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
?>