<?php
	include_once 'select.php';
	
	function createGroup($dbh, $id, $title) {
		try {
			$group = selectGroupById($dbh, $id);
			if(empty($group)) {
				$dbh->query("SET CHARACTER SET utf8");
				$sth = $dbh->prepare('INSERT INTO groups(ID_Groups, title) VALUES(?, ?)');
				$sth->execute(array($id, $title));
			}		
			else {
				throw new Exception('Неуникальный идентификатор группы');
			}
		}
		catch(PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}		
	}
	
	function createLesson($dbh, $id, $weekDay, $lessonNumber, $auditorium, $discipline, $type) {
		try {
			$lesson = selectLessonById($dbh, $id);
			if (empty($lesson)) {
				$dbh->query("SET CHARACTER SET utf8");
				$sth = $dbh->prepare('INSERT INTO lesson(ID_Lesson, week_day, lesson_number, auditorium, discipline, type) VALUES(?, ?, ?, ?, ?, ?)');
				$sth->execute(array($id, $weekDay, $lessonNumber, $auditorium, $discipline, $type));
			}		
			else {
				throw new Exception('Неуникальный идентификатор урока');
			}			
		}
		catch(PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
	
	function createTeacher($dbh, $id, $name) {
		try {
			$teacher = selectTeacherById($dbh, $id);
			if (empty($teacher)) {
				$dbh->query("SET CHARACTER SET utf8");
				$sth = $dbh->prepare('INSERT INTO teacher(ID_Teacher, name) VALUES(?, ?)');
				$sth->execute(array($id, $name));
			}		
			else {
				throw new Exception('Неуникальный идентификатор уителя');
			}			
		}
		catch(PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
?>