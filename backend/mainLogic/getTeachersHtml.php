<?php
	include_once '../backend/mainLogic/select.php';
	include_once '../backend/mainLogic/connection.php';
?>
<?php
	$dbh = connectToDb();
	$teachers = selectAllTeachers($dbh);
	foreach($teachers as $teacher) {
		echo "<option value='".$teacher['name']."'>".$teacher['name']."</option>";
	}				
	disconnectFromDb($dbh);
?>