<?php
	include_once 'select.php';
	include_once 'connection.php';
?>
<?php
	$dbh = connectToDb();
	if (array_key_exists('groupsTitles', $_GET)) {
		$selectedGroupTitle = $_GET['groupsTitles'];
		if (!empty($selectedGroupTitle)) {
			$group = selectGroupByTitle($dbh, $selectedGroupTitle);
			$groupId = $group['ID_Groups'];
			$selectedLessonsByGroup = selectLessonsForGroup($dbh, $groupId);
			$resultArray = array();
			foreach($selectedLessonsByGroup as $lesson) {
				array_push($resultArray, $lesson['discipline'].", ".$lesson['week_day']. ", ".$lesson['lesson_number']);
			}
			echo json_encode($resultArray);
		}
	}
	disconnectFromDb($dbh);
?>