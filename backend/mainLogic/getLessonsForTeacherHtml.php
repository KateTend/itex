<?php
	include_once 'select.php';
	include_once 'connection.php';
?>
<?php
	$dbh = connectToDb();
	if (array_key_exists('teachersNames', $_GET)) {
		$selectedTeacherName = $_GET['teachersNames'];
		if (!empty($selectedTeacherName)) {
			$teacher = selectTeacherByName($dbh, $selectedTeacherName);
			$teacherId = $teacher['ID_Teacher'];
			$selectedLessonsByTeacher = selectLessonsForTeacher($dbh, $teacherId);
			$resultArray = array();
			foreach($selectedLessonsByTeacher as $lesson) {
				array_push($resultArray, $lesson['discipline'].", ".$lesson['week_day']. ", ".$lesson['lesson_number']);
			}
			echo json_encode($resultArray);
		}
	}
	disconnectFromDb($dbh);
?>