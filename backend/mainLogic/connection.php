<?php
	function connectToDb() {
		$dsn = 'mysql:host=localhost;dbname=testdb';
		$username = 'root'; $password = '';
		$options = array( PDO:: MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
		$dbh = new PDO ($dsn, $username, $password, $options);
		return $dbh;
	}
	
	function disconnectFromDb($dbh) {
		$dbh = null;
	}	
?>