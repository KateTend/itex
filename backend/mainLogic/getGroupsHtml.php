<?php
	include_once '../backend/mainLogic/select.php';
	include_once '../backend/mainLogic/connection.php';
?>
<?php
	$dbh = connectToDb();
	$groups = selectAllGroups($dbh);
	foreach($groups as $group) {
		echo "<option value='".$group['title']."'>".$group['title']."</option>";
	}				
	disconnectFromDb($dbh);
?>