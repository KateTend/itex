<!DOCTYPE html>
<html>
<head>
</head>
<body>
<?php
	include_once '../mainLogic/creation.php';
	include_once '../mainLogic/connection.php';
	
	$dbh = connectToDb();
	
	$id = $_POST['lessonId'];
	$weekDay = $_POST['lessonWeekDay'];
	$lessonNumber = $_POST['lessonNumber'];
	$auditorium = $_POST['lessonAuditorium'];
	$discipline = $_POST['lessonDiscipline'];
	$type = $_POST['lessonType'];
	
	createLesson($dbh, $id, $weekDay, $lessonNumber, $auditorium, $discipline, $type);
	
	disconnectFromDb($dbh);		
	
	echo '<a href="../../frontend/creatingPage.html">Урок успешно добавлен, вернуться к добавлению</a>';
?>
<div><a href='../../frontend/creatingPage.html'>Назад на страницу добавленния</a></div>
<div><a href='../../frontend/mainPage.html'>Назад на головну</a></div>
		
</body>
</html>