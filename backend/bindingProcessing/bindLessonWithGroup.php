<?php
	include_once '../mainLogic/connection.php';
	include_once '../mainLogic/bindings.php';
	include_once '../mainLogic/select.php';
	
	$groupTitle = $_POST['groupsTitles'];
	$lessonTitle = $_POST['lessonsTitles'];
	
	$dbh = connectToDb();
	
	$selectedLesson = selectLessonByDisciple($dbh, $lessonTitle);
	$selectedGroup = selectGroupByTitle($dbh, $groupTitle);
	
	$idLesson = $selectedLesson['ID_Lesson'];
	$idGroup = $selectedGroup['ID_Groups'];
	
	addLessonToGroup($dbh, $idLesson, $idGroup);
	
	echo '<a href="../../frontend/mainPage.html">Binding created, return to main page</a>';
	
	disconnectFromDb($dbh);
?>