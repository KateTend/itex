<?php
	include_once '../mainLogic/connection.php';
	include_once '../mainLogic/bindings.php';
	include_once '../mainLogic/select.php';
	
	$teacherName = $_POST['teachersNames'];
	$lessonTitle = $_POST['lessonsTitles'];
	
	$dbh = connectToDb();
	
	$selectedLesson = selectLessonByDisciple($dbh, $lessonTitle);
	$selectedTeacher = selectTeacherByName($dbh, $teacherName);
	
	$idLesson = $selectedLesson['ID_Lesson'];
	$idTeacher = $selectedTeacher['ID_Teacher'];
	
	addLessonToTeacher($dbh, $idLesson, $idTeacher);
	
	echo '<a href="../../frontend/mainPage.html">Binding created, return to main page</a>';
	
	disconnectFromDb($dbh);
?>